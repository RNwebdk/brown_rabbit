$(document).ready(function(){
	//bxslider
  $('.bxslider').bxSlider();

  $('#forside').show();
   $('#events, #about, #nordic, #results, #other, #contact').hide();
});

//navigation
function about(){
	$('#about').show();
	$('#forside, #events, #nordic, #results, #other, #contact').hide();
}
function events(){
	$('#events').show();
	$('#forside, #about, #nordic, #results, #other, #contact').hide();
}
function nordic(){
	$('#nordic').show();
	$('#forside, #about, #events, #results, #other, #contact').hide();
}
function results(){
	$('#results').show();
	$('#forside, #about, #events, #nordic, #other, #contact').hide();
}
function other(){
	$('#other').show();
	$('#forside, #about, #events, #nordic, #results, #contact').hide();
}

function contact(){
	$('#contact').show();
	$('#forside, #about, #events, #nordic, #results, #other').hide();
	
}